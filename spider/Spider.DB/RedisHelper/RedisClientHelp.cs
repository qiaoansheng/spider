﻿using StackExchange.Redis;
using System.Configuration;

namespace Spider.DB.RedisHelper
{
    public class RedisClientHelp
    {
        private RedisClientHelp() { }
        private static RedisClientHelp redisClickHelp;
        private static ConnectionMultiplexer conn;
        private static object o = new object();
        private static string connect = ConfigurationManager.AppSettings["RedisConnect"];
        public static RedisClientHelp Instance()
        {
            if (redisClickHelp == null)
            {
                lock (o)
                {
                    if (redisClickHelp == null)
                    {
                        redisClickHelp = new RedisClientHelp();
                        conn = ConnectionMultiplexer.Connect(connect);
                    }
                }
            }
            return redisClickHelp;
        }

        /// <summary>
        /// 返回Redis连接实例
        /// </summary>
        /// <returns></returns>
        public ConnectionMultiplexer GetRedisClient()
        {
            return conn;
        }
    }
}
