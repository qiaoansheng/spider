﻿using MongoDB.Driver;

namespace Spider.DB.MongoHelper
{
    public class MongoDBBase
    {
        private IMongoDatabase db = null;
        public IMongoDatabase Db { get => db; }
        public MongoDBBase()
        {
            db = MongoDbManager.CreateDb();
        }
    }
}
