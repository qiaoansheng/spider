﻿using MongoDB.Driver;

namespace Spider.DB.MongoHelper
{
    public class MongoDbManager
    {
        private static IMongoDatabase db = null;
        private static readonly object locker = new object();
        /// <summary>
        /// 使用单列模式创建连接
        /// </summary>
        /// <returns></returns>
        public static IMongoDatabase CreateDb()
        {
            if (db == null)
            {
                lock (locker)
                {
                    if (db == null)
                    {
                        MongoClient Client = new MongoClient(MongoDbConfig.Host);
                        db = Client.GetDatabase(MongoDbConfig.DataBase);
                    }
                }
            }
            return db;
        }
    }
}
