﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spider.DB
{
    public static class DbManager
    {
        private static MongoDb mongoDB = new MongoDb();
        public static MongoDb InstanceMongoDB()
        {
            return mongoDB;
        }

        private static RedisDb redisDB = new RedisDb();
        public static RedisDb InstanceRedisDB()
        {
            return redisDB;
        }
    }
}
