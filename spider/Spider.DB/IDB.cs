﻿namespace Spider.DB
{
    public interface IDB
    {
        T Get<T>(string key, object obj);
        bool Set<T>(string key, T t) where T : class;
        bool Remove(string key);
    }
}
