﻿using Spider.Model.Spider;

namespace Spider.Core.Downloader
{
    public interface IDownloader
    {
        ResponseModel Download(RequestModel request);
    }
}
