﻿using Spider.Core.Downloader;
using Spider.Core.Pipeline;
using Spider.Core.Spiders;

namespace Spider.Core.Engines
{
    /// <summary>
    /// 爬虫引擎规则
    /// </summary>
    public interface ISpiderEngine
    {
        /// <summary>
        /// 下载器
        /// </summary>
        IDownloader Downloader { get; set; }

        /// <summary>
        /// 蜘蛛爬虫
        /// </summary>
        ISpider Spider { get; set; }

        /// <summary>
        /// 消息管道
        /// </summary>
        IPipeline Pipeline { get; set; }

        /// <summary>
        /// 开始
        /// </summary>
        void Start();

        /// <summary>
        /// 暂停
        /// </summary>
        void Pause();

        /// <summary>
        /// 停止
        /// </summary>
        void Stop();

        /// <summary>
        /// 继续
        /// </summary>
        void Continue();

        /// <summary>
        /// 添加管道处理
        /// </summary>
        /// <param name="handle"></param>
        void AddPipelineHandle(IPipelineHandler handle);

        /// <summary>
        /// 移除管道处理
        /// </summary>
        void RemovePipelineHandle(IPipelineHandler handle);

    }
}
