﻿using Spider.Core.Downloader;
using Spider.Core.Pipeline;
using Spider.Core.Scheduler;
using Spider.Core.Spiders;
using System.Threading;

namespace Spider.Core.Engines
{
    public class SpiderEngine
    {
        #region 属性
        private readonly Thread _oneSpiderEngineThread;

        /// <summary>
        /// 下载器
        /// </summary>
        public IDownloader Downloader { get; set; }

        /// <summary>
        /// 调度器
        /// </summary>
        public IScheduler Scheduler { get; set; }

        /// <summary>
        /// 蜘蛛模板
        /// </summary>
        public ISpider Spider { get; set; }

        /// <summary>
        /// 消息管道
        /// </summary>
        public IPipeline Pipeline { get; set; }

        /// <summary>
        /// 开启最大线程数
        /// </summary>
        private int ThreadMaxCount;

        /// <summary>
        /// 线程数组
        /// </summary>
        private Thread[] CrawlThreads;

        /// <summary>
        /// 爬虫统计
        /// </summary>
        private static int spiderCount;

        /// </summary>
        #endregion

        #region 初始化

        #region 构造函数 初始化默认  下载器、爬虫、管道 各种属性
        /// <summary>
        /// 初始化爬虫引擎
        /// </summary>
        /// <param name="threadCount"></param>
        public SpiderEngine(int threadCount = 10)
        {
            //初始化爬虫最大线程数
            this.ThreadMaxCount = threadCount;
            CrawlThreads = new Thread[threadCount];
            Downloader = new HttpDownloader();
            Spider = new Spider.Core.Spiders.Spider();
            Pipeline = new Pipelinehandler();

            #region 初始化 抓取线程
            for (int i = 0; i < ThreadMaxCount; i++)
            {
                CrawlThreads[i] = new Thread(new ParameterizedThreadStart(LoopSchedulerPop));
            }
            #endregion

        }
        #endregion

        #region 初始化管道处理模型
        public void AddPipelineHandle(IPipelineHandler handle)
        {
            Pipeline.AddPipelineHandle(handle);
        }

        public void RemovePipelineHandle(IPipelineHandler handle)
        {
            Pipeline.RemovePipelineHandle(handle);
        }
        #endregion

        #endregion


        
        private void LoopSchedulerPop(object index)
        {
            while (true)
            {
                ////从调度器中 弹出一条数据
                var request = Scheduler.Pop();
                if (request == null)
                {
                    continue;
                }
                ////调用下载器
                var response = Downloader.Download(request);
                if (response != null)
                {
                    //爬虫模板解析响应对象
                    var message = Spider.Extract(response);
                    //管道处理请求
                    Pipeline.Push(message);
                }
            }
        }


    }
}
