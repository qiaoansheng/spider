﻿namespace Spider.Core.Scheduler
{
    public class SchedulerManage
    {
        private readonly static IScheduler Scheduler = new RedisScheduler();
        public static IScheduler Instance
        {
            get
            {
                return Scheduler;
            }
        }
    }
}
