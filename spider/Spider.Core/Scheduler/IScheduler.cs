﻿using Spider.Model.Spider;

namespace Spider.Core.Scheduler
{
    /// <summary>
    /// 调度器
    /// </summary>
    public interface IScheduler
    {
        /// <summary>
        /// 推送
        /// </summary>
        bool Push(RequestModel request);

        /// <summary>
        /// 弹出
        /// </summary>
        RequestModel Pop();


        void Switch();
    }
}
