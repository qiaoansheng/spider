﻿using Spider.DB;
using Spider.Model.Spider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Spider.Core.Spiders
{
    public class Spider : ISpider
    {
        #region 属性 字段

        /// <summary>
        /// 数据持久化
        /// </summary>
        private readonly IDB db = new RedisDb();

        /// <summary>
        /// 需要排除的 URL后缀  
        /// 例如 .ico .css .pdf .xml
        /// </summary>
        static List<string> UrlExt = new List<string>()
        {
            ".ico",
            ".css",
            ".xml"
        };

        #endregion

        #region 判断URL是否存在
        /// <summary>
        /// 判断URL是否存在
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public bool Exist(string url)
        {
            var key = db.Get<string>(url, null);
            return string.IsNullOrEmpty(key) == false;
        }
        #endregion

        #region 提取URL内容
        /// <summary>
        /// 提取URL内容
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public MessageContext Extract(ResponseModel response)
        {
            return ExtractHtmlResponse(response as HtmlResponseModel);
        }
        #endregion

        #region 解析Html内容
        /// <summary>
        /// 解析Html内容
        /// </summary>
        /// <param name="htmlResponse"></param>
        /// <returns></returns>
        private MessageContext ExtractHtmlResponse(HtmlResponseModel htmlResponse)
        {
            List<string> urlList = new List<string>();
            //判断是否提取文档链接
            if (htmlResponse.Request.IsExtractLink)
            {
                urlList = ExtractLink(htmlResponse);
            }

            var message = new MessageContext
            {
                Response = htmlResponse.Clone() as ResponseModel,
                Html = htmlResponse.Html,
                UrlList = urlList
            };
            return message;
        }
        #endregion

        #region 提取文档链接
        /// <summary>
        /// 提取文档链接
        /// </summary>
        /// <param name="htmlResponse"></param>
        private List<string> ExtractLink(HtmlResponseModel htmlResponse)
        {
            //解析出全部Url并去除重复记录
            var urlList = GetHrefUrlList(htmlResponse.Html, htmlResponse.Request.Url).Distinct();

            //过滤第三方网站域名
            if (htmlResponse.Request.IgnoreThirdpartyDomain)
            {
                var uriHost = new Uri(htmlResponse.Request.Url).Host.Replace("www.", "");

                urlList = urlList.Where(p => p.Contains(uriHost)).ToList();
            }

            List<string> UrlList = new List<string>();
            //循环处理url列表
            foreach (var url in urlList)
            {
                try
                {
                    var urlHash = url.ToMD5_UTF8();
                    var redisKey = string.Format("spider:url:{0}", urlHash);

                    //是否满足抓取条件
                    if (Exist(redisKey) == false)
                    {
                        UrlList.Add(url);
                    }
                }
                catch (Exception exception)
                {

                }
            }
            return UrlList;
        }
        #endregion

        #region 链接正则：href="[\s\S\W]+?"
        /// <summary>
        /// 链接正则：href="[\s\S\W]+?"
        /// </summary>
        /// <param name="html"></param>
        /// <param name="rootUrl"></param>
        /// <returns></returns>
        private static IEnumerable<string> GetHrefUrlList(string html, string rootUrl)
        {
            if (string.IsNullOrEmpty(html)) return new List<string>();
            var uri = new Uri(rootUrl);
            var result = new List<string>();
            var matchCollection = Regex.Matches(html, "href=\"[\\s\\S\\W]+?\"", RegexOptions.IgnoreCase);
            for (var i = 0; i < matchCollection.Count; i++)
            {
                var href = matchCollection[i].Value;

                if (href.Length < 6) continue;

                //if (UrlExt.Contains(href)) continue;
                ///需要把一些无用后缀的URL请求给屏蔽了

                href = href.Substring(6, href.Length - 7);

                if (href.StartsWith("#", StringComparison.CurrentCultureIgnoreCase) || href == "/") continue;

                if (href.StartsWith("javascript:", StringComparison.CurrentCultureIgnoreCase)) continue;

                if (href.StartsWith("//", StringComparison.CurrentCultureIgnoreCase))
                {
                    href = String.Format("http:{0}", href);

                    result.Add(href);
                }

                if (href.StartsWith("/", StringComparison.CurrentCultureIgnoreCase))
                {
                    //补http协议

                    href = String.Format("{0}://{1}/{2}", uri.Scheme, uri.Host, href.TrimStart('/'));

                    result.Add(href);

                    continue;
                }

                if (href.StartsWith("http://", StringComparison.CurrentCultureIgnoreCase) ||
                    href.StartsWith("https://", StringComparison.CurrentCultureIgnoreCase))
                {
                    //直接返回

                    result.Add(href);

                    continue;
                }
                //other
            }
            return result;
        }
        #endregion

    }
}
