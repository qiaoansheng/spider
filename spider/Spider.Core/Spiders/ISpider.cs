﻿using Spider.Model.Spider;

namespace Spider.Core.Spiders
{
    public interface ISpider
    {
        /// <summary>
        /// 提取内容
        /// </summary>
        MessageContext Extract(ResponseModel response);

        /// <summary>
        /// 是否已存在
        /// </summary>
        bool Exist(string url);
    }
}
