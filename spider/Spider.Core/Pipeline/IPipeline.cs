﻿using Spider.Model.Spider;

namespace Spider.Core.Pipeline
{
    public interface IPipeline
    {
        void Push(MessageContext message);

        /// <param name="handle">增加管道处理</param>
        void AddPipelineHandle(IPipelineHandler handle);

        /// <param name="handle">移除管道处理</param>
        void RemovePipelineHandle(IPipelineHandler handle);
    }
}
