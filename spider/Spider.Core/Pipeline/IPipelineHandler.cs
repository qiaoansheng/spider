﻿using Spider.Model.Spider;

namespace Spider.Core.Pipeline
{
    public interface IPipelineHandler
    {
        /// <summary>
        /// 过滤路径规则
        /// </summary>
        bool Filter(MessageContext message);

        /// <summary>
        /// 内容提取
        /// </summary>
        /// <param name="message">消息内容</param>
        void Extract(MessageContext message);
    }
}
