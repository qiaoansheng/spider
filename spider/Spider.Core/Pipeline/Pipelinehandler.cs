﻿using Spider.Model.Spider;
using System.Collections.Generic;

namespace Spider.Core.Pipeline
{
    public class Pipelinehandler : IPipeline
    {
        private readonly List<IPipelineHandler> _pipelineHandleList = new List<IPipelineHandler>();

        public void Push(MessageContext message)
        {
            foreach (var handle in _pipelineHandleList)
            {
                if (handle.Filter(message))
                {
                    handle.Extract(message);
                }
            }
        }

        public void AddPipelineHandle(IPipelineHandler handle)
        {
            _pipelineHandleList.Add(handle);
        }

        public void RemovePipelineHandle(IPipelineHandler handle)
        {
            _pipelineHandleList.Remove(handle);
        }
    }
}
