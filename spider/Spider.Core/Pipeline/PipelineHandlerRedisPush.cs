﻿using Newtonsoft.Json;
using Spider.Core.Scheduler;
using Spider.DB;
using Spider.Model.Spider;
using System;

namespace Spider.Core.Pipeline
{
    public class PipelineHandlerRedisPush : IPipelineHandler
    {
        /// <summary>
        /// 数据持久化
        /// </summary>
        private readonly IDB db = new RedisDb();

        /// <summary>
        /// 数据过滤
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool Filter(MessageContext message)
        {
            if (message != null &&
                message.UrlList != null &&
                message.UrlList.Count > 0)
            {
                ///只匹配 UrlList 中有数据的
                return true;
            }
            return false;
        }

        public void Extract(MessageContext message)
        {
            foreach (var url in message.UrlList)
            {
                try
                {
                    var urlHash = url.ToMD5_UTF8();
                    var redisKey = string.Format("spider:url:{0}", urlHash);

                    //是否满足抓取条件
                    var requestUri = new Uri(url);
                    var request = message.Response.Request.Clone() as RequestModel;

                    if (request != null && requestUri.IsFile == false)
                    {
                        request.Url = url;
                        request.Referer = message.Response.Request.Url;
                        request.Depth = ++message.Response.Request.Depth;

                        //推送到调度队列
                        SchedulerManage.Instance.Push(request);

                        var redisValue = JsonConvert.SerializeObject(request);
                        //推送到缓存
                        db.Set(redisKey, redisValue);
                    }
                }
                catch (Exception exception)
                {
                }
            }

        }

    }
}
