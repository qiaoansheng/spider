﻿using Spider.DB;
using Spider.Model.MongoDB;
using Spider.Model.Spider;
using System;

namespace Spider.Core.Pipeline
{
    public class PipelineHandlerMongoDB : IPipelineHandler
    {
        /// <summary>
        /// 初始化日志类型
        /// </summary>
        IDB db = new MongoDb();

        /// <summary>
        /// 过滤规则
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool Filter(MessageContext message)
        {
            ///所有的页面全部入库
            return true;
        }

        /// <summary>
        /// 处理 把页面持久化到 MongoDB 中
        /// </summary>
        /// <param name="message"></param>
        public void Extract(MessageContext message)
        {
            db.Set<listHTML>("", new listHTML()
            {
                Html = message.Html,
                FenLei = "",
                Source = message.Response.Request.Source,
                Url = message.Response.Request.Url,
                CreateTime = DateTime.Now,
                Request = message.Response.Request
            });
        }


    }
}
