﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spider.Model.Spider
{
    /// <summary>
    /// 请求对象
    /// </summary>
    public class RequestModel : ICloneable
    {
        public RequestModel()
        {
            Header = new List<KeyValue>();
            Cookie = new List<KeyValue>();
        }

        /// <summary>
        /// Url地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 页面抓取的来源网站
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// UrlMd5签名
        /// </summary>
        public string UrlHash { get; set; }

        /// <summary>
        /// 抓取深度
        /// </summary>
        public int Depth { get; set; }

        /// <summary>
        /// Header集合
        /// </summary>
        public List<KeyValue> Header { get; set; }

        /// <summary>
        /// Cookie集合
        /// </summary>
        public List<KeyValue> Cookie { get; set; }

        /// <summary>
        /// Http请求方法
        /// </summary>
        public string Method { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Accept { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Connection { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public string Encoding { get; set; }

        /// <summary>
        /// 浏览器头
        /// </summary>
        public string UserAgent { get; set; }

        /// <summary>
        /// 内容类型
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// 主机
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// 来源
        /// </summary>
        public string Referer { get; set; }

        /// <summary>
        /// 默认超时30秒
        /// </summary>
        public int Timeout { get; set; }

        /// <summary>
        /// 默认休眠1000毫秒
        /// </summary>
        public int Sleep { get; set; }

        /// <summary>
        /// 忽略外部网站域名
        /// </summary>
        public bool IgnoreThirdpartyDomain { get; set; } = true;

        /// <summary>
        /// 是否提取链接，默认true
        /// </summary>
        public bool IsExtractLink { get; set; } = true;

        /// <summary>
        /// 抓取完成时间
        /// </summary>
        public DateTime CrawlTime { get; set; }

        /// <summary>
        /// 总下载时间毫秒
        /// </summary>
        public double DownloadTime { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
