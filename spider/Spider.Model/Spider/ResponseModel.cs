﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spider.Model.Spider
{
    /// <summary>
    /// 响应对象
    /// </summary>
    public class ResponseModel : ICloneable
    {
        /// <summary>
        /// 请求对象
        /// </summary>
        public RequestModel Request { get; set; }

        /// <summary>
        /// Header集合
        /// </summary>
        public List<KeyValue> Header { get; set; }

        /// <summary>
        /// Cookie集合
        /// </summary>
        public List<KeyValue> Cookie { get; set; }

        /// <summary>
        /// 页面信息
        /// </summary>
        public string Html { get; set; }

        public ResponseModel()
        {
            Request = new RequestModel();
            Header = new List<KeyValue>();
            Cookie = new List<KeyValue>();
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}

