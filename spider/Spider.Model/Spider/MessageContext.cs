﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spider.Model.Spider
{
    /// <summary>
    /// 爬虫解析完成后转换成消息对象
    /// </summary>
    public class MessageContext
    {
        /// <summary>
        /// 下载器相应对象
        /// </summary>
        public ResponseModel Response { get; set; }

        /// <summary>
        /// HTML 页面
        /// </summary>
        public string Html { get; set; }

        /// <summary>
        /// 解析出来的URL
        /// </summary>
        public List<string> UrlList { get; set; }

    }
}
