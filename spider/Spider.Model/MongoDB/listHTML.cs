﻿using Spider.Model.Spider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spider.Model.MongoDB
{
    public class listHTML
    {
        /// <summary>
        /// 页面URL地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 页面HTML
        /// </summary>
        public string Html { get; set; }

        /// <summary>
        /// 当前页面分类
        /// </summary>
        public string FenLei { get; set; }

        /// <summary>
        /// 页面抓取的来源网站
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// 插入时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 请求对象
        /// </summary>
        public RequestModel Request { get; set; }

    }
}
