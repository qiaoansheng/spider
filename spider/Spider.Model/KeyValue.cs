﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spider.Model
{
    /// <summary>
    /// KeyValue对
    /// </summary>
    public class KeyValue
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public KeyValue()
        {
            Name = String.Empty;
            Value = String.Empty;
        }

        public KeyValue(string name, string value)
        {
            Name = name;
            Value = value;
        }

        public override string ToString()
        {
            return string.Format("{0}.{1}", Name, Value);
        }
    }
}
